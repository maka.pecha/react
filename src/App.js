import React from 'react';
import faker from 'faker';
import './App.css';
import "semantic-ui-css/semantic.min.css";
import Message from './Message';
import Counter from "./Counter";

function App() {
  return (
    <div className="container">
      <Hello/>
      <div>
        <img src={faker.image.avatar()}/>
        <br/>
        <Message children="Hola Mundo Incluit y Simtlix!"/>
        <Message children="Animate e incrementa el contador"/>
        <Counter/>
      </div>
    </div>
  );
}

const Hello = () => {
  const name='Maka Pecha';
  return <div id="Hello"> Hello my name is {name}! </div>
};

export default App;
