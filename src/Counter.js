import React, {useState} from 'react'
import './Counter.css'

const Counter = props => {
  const [counter, setCounter] = useState(0);
  const updateCounter = () => setCounter(counter + 1);

  return (
    <div>
      {counter}
      <button onClick={updateCounter}>Incrementar</button>
    </div>
  )

  const {children} = props;
  return(<p>{children}</p>);
};
export default Counter;