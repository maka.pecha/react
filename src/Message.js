import React from 'react'

const Message = props => {
  const {children} = props;
  return(<p>{children}</p>);
};
export default Message;